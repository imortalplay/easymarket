package com.example.easymarket.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.security.keystore.UserPresenceUnavailableException;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.easymarket.R;
import com.example.easymarket.helper.ConfiguracaoFirebase;
import com.example.easymarket.model.Usuario;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.UserProfileChangeRequest;

public class CadastrarActivity extends AppCompatActivity {

    private EditText campoNome, campoEmail, campoSenha, campoConfSenha;
    private Button botaoCadastrar;
    private ProgressBar progressBar;

    private Usuario usuario;

    private FirebaseAuth autenticacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar);

        inicializarComponentes();

        //configurando toolbar cadastro
        Toolbar toolbar = findViewById(R.id.toolbarPrincipal);
        toolbar.setTitle("Cadastrar");
        toolbar.setTitleTextColor(0xFFFFFFFF);
        setSupportActionBar( toolbar );

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Cadastrar usuario
        progressBar.setVisibility(View.GONE);
        botaoCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Da get nos inputs
                String textoNome          = campoNome.getText().toString();
                String textoEmail         = campoEmail.getText().toString();
                String textoSenha         = campoSenha.getText().toString();
                String textoConfirmaSenha = campoConfSenha.getText().toString();

                if( !textoNome.isEmpty() ){
                    if( !textoEmail.isEmpty() ){
                        if( !textoSenha.isEmpty() ){
                            if( !textoConfirmaSenha.isEmpty() ){

                                //Esconde o teclado do aparelho
                                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                                if(imm.isActive()){
                                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                                }

                                usuario = new Usuario();
                                usuario.setNome( textoNome );
                                usuario.setEmail( textoEmail );
                                usuario.setSenha( textoSenha );
                                cadastrar( usuario );

                            }else{
                                Toast.makeText(CadastrarActivity.this,
                                        "Confirme a senha!",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(CadastrarActivity.this,
                                    "Preencha a senha!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(CadastrarActivity.this,
                                "Preencha o Email!",
                                Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(CadastrarActivity.this,
                            "Preencha o nome!",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    //Metodo responsavel por cadastrar usuario e fazer validações
    public void cadastrar( Usuario usuario ){

        progressBar.setVisibility(View.VISIBLE);
        autenticacao = ConfiguracaoFirebase.getFirebaseAutenticacao();
        autenticacao.createUserWithEmailAndPassword(
                usuario.getEmail(),
                usuario.getSenha()
        ).addOnCompleteListener(
                this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if( task.isSuccessful() ){

                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(CadastrarActivity.this,
                                    "Usuario cadastrado com sucesso",
                                    Toast.LENGTH_SHORT).show();

                            startActivity( new Intent(getApplicationContext(), BoasVindasActivity.class));
                            finish();

                        }else{

                            progressBar.setVisibility( View.GONE );

                            String erroExcecao = "";
                            try{
                                throw task.getException();
                            }catch (FirebaseAuthWeakPasswordException e){
                                erroExcecao = "Digite uma senha mais forte!";
                            }catch (FirebaseAuthInvalidCredentialsException e){
                                erroExcecao = "Email ou senha invalidos!";
                            }catch (FirebaseAuthUserCollisionException e){
                                erroExcecao = "Está email já foi cadastrado!";
                            }catch (Exception e) {
                                erroExcecao = "ao cadastrar usuário: " + e.getMessage();
                                e.printStackTrace();
                            }

                            Toast.makeText(CadastrarActivity.this,
                                    "Erro: " + erroExcecao ,
                                    Toast.LENGTH_SHORT).show();

                        }
                    }
                }
        );
    }

    public void inicializarComponentes(){

        campoNome      = findViewById(R.id.editCadastroNome);
        campoEmail     = findViewById(R.id.editCadastroEmail);
        campoSenha     = findViewById(R.id.editCadastroSenha);
        campoConfSenha = findViewById(R.id.editCadastroSenha2);
        botaoCadastrar = findViewById(R.id.buttonCadastrar);
        progressBar    = findViewById(R.id.progressCadastrar);

    }

}
