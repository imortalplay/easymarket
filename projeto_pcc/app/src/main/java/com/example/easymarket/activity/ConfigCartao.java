package com.example.easymarket.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.easymarket.R;

public class ConfigCartao extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config_cartao);

        //configurando toolbar cadastro
        Toolbar toolbar = findViewById(R.id.toolbarPrincipal);
        toolbar.setTitle("Configurações do cartão");
        toolbar.setTitleTextColor(0xFFFFFFFF);
        setSupportActionBar( toolbar );

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void abrirAdicionarCartao(View v){
        startActivity(new Intent(getBaseContext(), AdicionarCartao.class));
    }
}
